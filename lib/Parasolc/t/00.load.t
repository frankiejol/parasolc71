use Test::More tests => 1;

BEGIN {
use_ok( 'Parasolc' );
}

diag( "Testing Parasolc $Parasolc::VERSION" );
