use Test::More;# tests => 10;

use strict;
use warnings;

use HTML::Parser;
use Text::Diff;

use Parasolc qw(autor_curs strip_html crea_tex);

sub test_txt {
    my $font = 'fonts/eso/PRAGA 2013.txt';
    my $file_out = 'eso/905_praga_2013.tex';
    do_test($font, $file_out);
}

sub test_txt1 {
    my $font = 'fonts/eso/PRAGA_2013_1.txt';
    my $file_out = 'eso/905_praga_2013_1.tex';
    if (! -e $font ) {
        warn "Falta $font en git\n";
        return;
    }
    do_test($font, $file_out);
}

sub test_txt2 {
    my $font = 'fonts/eso/PRAGA_2013_2.txt';
    my $file_out = 'eso/900_praga_2013_2.tex';
    do_test($font, $file_out);
}



sub clean {
    my $file = shift;

    unlink $file or die "$! $file"
        if -e $file;

    my ($dir) = $file =~ m   {(.*)/};
#    rmdir $dir or die "$! $dir"
#        if -d $dir;
    opendir my $ls, $dir or return;
    while (my $file = readdir $ls) {
#        warn "$file\n";
        next if ! -f "$dir/$file";
        unlink "$dir/$file" or die "$! $file";
#        warn "unlinked";
    }
    closedir $ls;

}

sub do_test {
    my ($font,$file_out) = @_;

    clean($file_out);
    
    my $file_tex = crea_tex($font,$font);
    
    ok($file_tex);
    ok($file_tex eq $file_out,"tex: got $file_tex, exp: $file_out") and do {
    
        my $file_expected = "expected/$file_tex";
        die "Falta $file_expected" if ! -e $file_expected;
        my $diff = diff($file_tex,"$file_expected");
        ok(!$diff,$diff) or exit;
    };
    unlink $file_tex;
}

sub test_utf {
    my $file_utf = "fonts/angles/My favourite place Anna Basquet.txt";
    if (! -e $file_utf ) {
        warn "Falta $file_utf a git\n";
        return;
    }
    do_test($file_utf
            ,"angles/405_favourite_place_anna_basquet.tex");
}

sub do_test_html {
    my $file = shift;

    my $dir_out = 'out';

    my $file_out_exp = $file;
    $file_out_exp =~ s{.*/(.*)html$}{$dir_out/$1txt};

    unlink $file_out_exp or die "$! $file_out_exp"
        if -e $file_out_exp;

    my $file_out = strip_html($file , $dir_out);
    ok(-e $file_out);

    ok($file_out eq $file_out_exp,"Expecting $file_out_exp, got $file_out") or return;

    diag($file_out);
    my $expected = $file;
    $expected =~ s{(.*)(\..*)}{${1}_exp.txt};

    if ( ! -e $expected ) {
        die "Falta arxiu '$expected'"   ;
        return;
    }
    my $diff = diff($file_out, $expected);
    ok(!$diff,$diff);
    return $file_out;
}

sub do_test_tex {
    my $file_font = shift;
    my $file_tex = shift;

    my $exp_file_out = $file_font;

    my $n = Parasolc::prefix_seccio($file_font);

    $exp_file_out =~ s{fonts/(.*)/(.*)\..*}{$1/${n}05_$2.tex};
    clean($exp_file_out);

    my $expected = $file_font;
    $expected =~ s{(.*)(\..*)}{${1}_exp.tex};

    my $file_out = crea_tex($file_font, $file_tex);
    ok($exp_file_out eq $file_out,"Expecting $exp_file_out, got $file_out") or return;
    if ( ! -e $expected ) {
        die "Falta arxiu '$expected'"   ;
        return;
    }
    my $diff = diff($file_out, $expected);
    ok(!$diff,$diff) && do {
        unlink $file_out or die "$! $file_out";
    };

}

sub test_basquet1 {
    do_test_html("fonts/basquet.html");
####    do_test_html("fonts/basquet2.html");
    do_test_html("fonts/basquet3.html");
    do_test_html("fonts/basquet4.html");
#    do_test_html("fonts/basquet5.html");
}

sub test_bcn {
#    $Parasolc::DEBUG=1;
    run_test("fonts/eso/bcn.html");
}

sub run_test {
    my $font = shift;
    my $file_txt = do_test_html($font);
    do_test_tex($font , $file_txt);
}

###################################################

run_test("fonts/primaria/taller.html");

test_bcn();
done_testing;
exit;

test_basquet1();
test_txt1();
#test_txt();
test_utf();
do_test_html("fonts/ramon.html");
do_test_html("fonts/taller.html");
done_testing();

