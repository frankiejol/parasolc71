use Test::More tests => 21;

use warnings;
use strict;
use utf8;

use Parasolc qw(autor_curs possible_curs);

{
my ($autor,$curs) = autor_curs();

ok(!$autor);
ok(!$curs);
}


{
my $escola_data ='Escola Solc 11-1-2013';
my ($autor,$curs) = autor_curs($escola_data);
ok(!$autor);
ok($curs eq $escola_data);
}

{
my $escola = 'Escola Solc';
my ($autor,$curs) = autor_curs($escola);
ok(!$autor);
ok($curs eq $escola);
}

{
    my ($pre_autor,$pre_curs) = ('Pepe Perez',"1er d'ESO");
    my ($autor,$curs) = autor_curs($pre_curs,$pre_autor);
    ok($autor && $autor eq $pre_autor, "trobat :".($autor or '<undef>')." exp=$pre_autor");
    ok($curs && $curs eq $pre_curs,"trobat :".($curs or '<undef>')." exp=$pre_curs");
}

{
    my ($pre_autor,$pre_curs) = ('Pepe Perez',"1er d'ESO");
    my ($autor,$curs) = autor_curs("$pre_autor, $pre_curs");
    ok($autor && $autor eq $pre_autor, "autor: ".($autor or '<undef>')." exp=$pre_autor");
    ok($curs && $curs eq $pre_curs,"curs:".($curs or '<undef>')." exp=$pre_curs");

}

{
    my $linia = "Els nois i les noies de 4t d'ESO vam anar a Praga";
    my ($autor,$curs) = autor_curs($linia);
    ok(!$autor);
    ok($curs && $curs eq "4t d'ESO","curs: ".($curs or '<undef>')." , exp: '4t d'ESO'");
}

{
#    $Parasolc::DEBUG=1;
    my $linia = "Els nois i les noies de TERCER DE PRIMÀRIA vam anar a Praga";
    ok(possible_curs($linia)) and do {
        my ($autor,$curs) = autor_curs($linia);
        ok(!$autor);
        my $exp = 'Tercer de Primària';
        ok($curs && $curs eq $exp,"curs: ".($curs or '<undef>')." , exp: '$exp'");
    };
}

{
#    $Parasolc::DEBUG=1;
    my $linia = "Coordinació d'Escola";
    ok(possible_curs($linia)) and do {
        my ($autor,$curs) = autor_curs($linia);
        ok(!$autor);
        my $exp = $linia;
        ok($curs && $curs eq $exp,"curs: ".($curs or '<undef>')." , exp: '$exp'");
    };
}

{
    my $linia="Enric Larreula va venir a la classe de 3r d’ESO per fer-nos una xerrada";
    ok(possible_curs($linia)) and do {
       my ($autor,$curs) = autor_curs($linia);
        ok(!$autor);
        my $exp = "3r d’ESO";
        ok($curs && $curs eq $exp,"curs: ".($curs or '<undef>')." , exp: '$exp'");

    };
}

