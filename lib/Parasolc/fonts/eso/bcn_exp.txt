
BCN Medieval

El dijous 5 de febrer, els nois i noies de 2n d’ESO vam anar a conèixer la Barcelona medieval. Vam visitar els diversos carrers dels gremis (com el dels assaonadors, els teixidors, els corders), Santa Maria del Mar (un dels màxims exponents d’estil gòtic català), algunes cases de nobles (com les del carrer Montcada) i vam poder entrar a alguna part del Palau Reial de Barcelona (el Saló del Tinell).



Ferran Corral
2n d’ESO

