use strict;
use warnings;
use ExtUtils::MakeMaker;

WriteMakefile(
    NAME                => 'Parasolc',
    AUTHOR              => 'Francesc Guasch <frankie@etsetb.upc.edu>',
    VERSION_FROM        => 'lib/Parasolc.pm',
    ABSTRACT_FROM       => 'lib/Parasolc.pm',
    PL_FILES            => {},
    PREREQ_PM => {
        'Test::More' => 0,
        'version'    => 0,
    },
    dist                => { COMPRESS => 'gzip -9f', SUFFIX => 'gz', },
    clean               => { FILES => 'Parasolc-*' },
);
