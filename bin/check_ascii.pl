#!/usr/bin/perl -w

use warnings;
use strict;

die "$0 file1 file2 ... filen\n"	if !$ARGV[0];

###################################################

sub check_ascii {
	my $text = shift;

	my $ret = '';
	for (0 .. length($text)) {
		my $char = substr($text,$_,1);
        print "$char: ".ord($char)."\n";
		next if ord($char)<128;
		my $text2 = $text;
		substr($text,$_,1,'*');
		$ret = " char $_: $char ".ord($char)."\n$text\n$text2\n";
	}
	return $ret;
}

for my $filename (@ARGV) {

	my $line = 0;
	my $error = '';
	open my $file , '<',$filename or die "$! $filename";
	while (my $text = <$file>) {
		$line++;
        next if $text !~ /les nostres ciutats/;
		$error = check_ascii($text);
		next if !$error;
		warn "$filename, line $line, $error\n";
	}
	close $file;
}
