#!/bin/bash
REL=70
DIR_DROP="$HOME/Dropbox/parasolc$REL/tex"
DIR_BUILD="$HOME/Documents/parasolc/parasolc$REL"

cd $DIR_DROP && git pull && git push
cd $DIR_BUILD && git pull && ./bin/genera_makefile.pl && make esborrany
